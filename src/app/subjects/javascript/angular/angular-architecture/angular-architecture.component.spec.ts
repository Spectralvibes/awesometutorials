import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngularArchitectureComponent } from './angular-architecture.component';

describe('AngularArchitectureComponent', () => {
  let component: AngularArchitectureComponent;
  let fixture: ComponentFixture<AngularArchitectureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AngularArchitectureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngularArchitectureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
