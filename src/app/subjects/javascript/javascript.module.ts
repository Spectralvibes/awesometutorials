import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasicsComponent } from './basics/basics.component';
import { AngularComponent } from './angular/angular.component';
import { AngularArchitectureComponent } from './angular/angular-architecture/angular-architecture.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [BasicsComponent, AngularComponent, AngularArchitectureComponent]
})
export class JavascriptModule { }
