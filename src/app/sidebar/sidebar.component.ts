import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: 'dashboard', title: 'Dashboard',  icon:'pe-7s-science', class: '' },
    { path: 'angular', title: 'Angular',  icon: 'devicon-angularjs-plain', class: '' },
    { path: '_________', title: '_________',  icon: '', class: '' },
    { path: 'home', title: 'Home',  icon: 'devicon-git-plain', class: '' },
    { path: 'user', title: 'User Profile',  icon:'pe-7s-user', class: '' },
    { path: 'table', title: 'Table List',  icon:'pe-7s-note2', class: '' },
    { path: 'typography', title: 'Typography',  icon:'pe-7s-news-paper', class: '' },
    { path: 'icons', title: 'Icons',  icon:'pe-7s-science', class: '' },
    { path: 'maps', title: 'Maps',  icon:'pe-7s-map-marker', class: '' },
    { path: 'notifications', title: 'Notifications',  icon:'pe-7s-bell', class: '' },
];

export const angularROUTES: RouteInfo[] = [
    { path: 'dashboard', title: 'Dashboard',  icon:'pe-7s-science', class: '' },
    { path: 'angular', title: 'angular',  icon: 'devicon-angularjs-plain', class: '' },
    { path: 'angular-architecture', title: 'Architecture',  icon: 'devicon-angularjs-plain', class: '' },
    { path: 'angular-typescript', title: 'Typescript',  icon:'devicon-angularjs-plain', class: '' },
    { path: 'angular-process', title: 'How angular works',  icon:'devicon-angularjs-plain', class: '' },
    { path: 'angular-directives', title: 'Directives',  icon:'devicon-angularjs-plain', class: '' },
    { path: 'angular-forms', title: 'Forms in Angular',  icon:'devicon-angularjs-plain', class: '' },
    { path: 'angular-di', title: 'Dependancy Injection',  icon:'devicon-angularjs-plain', class: '' },
    { path: 'angular-http', title: 'HTTP service',  icon:'devicon-angularjs-plain', class: '' },
    { path: 'angular', title: 'Data architecture',  icon:'devicon-angularjs-plain', class: '' },
    { path: 'angular-observable-rxjs', title: 'Observables and RxJS',  icon:'devicon-angularjs-plain', class: '' },
    { path: 'angular-redux', title: 'Redux with typescript',  icon:'devicon-angularjs-plain', class: '' },
    { path: 'angular-testing', title: 'Testing',  icon:'devicon-angularjs-plain', class: '' },
];


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  
  getRoutes(slectedMenuItem) {
    if (slectedMenuItem==="angular"){
      this.menuItems = angularROUTES.filter(menuItem => menuItem);  
    }
    if (slectedMenuItem==="dashboard"){
      this.menuItems = ROUTES.filter(menuItem => menuItem);  
    }
  }
  
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
