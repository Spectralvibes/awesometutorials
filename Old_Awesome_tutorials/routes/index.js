var express = require('express');
var router = express.Router();


/* GET index page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/angularjs', function(req, res, next) {
  res.render('angularjs');
});

router.get('/java', function(req, res, next) {
  res.render('java');
});

router.get('/nodejs', function(req, res, next) {
  res.render('nodejs');
});

router.get('/tryit', function(req, res, next) {
  res.render('tryit');
});

router.get('/javascript', function(req, res, next) {
  res.render('javascript');
});

router.get('/jquery', function(req, res, next) {
  res.render('jquery');
});

router.get('/interview', function(req, res, next) {
  res.render('interview');
});

module.exports = router;
