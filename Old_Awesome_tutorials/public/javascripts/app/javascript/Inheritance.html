<div class='text'>
    <p>
        JavaScript is a bit confusing for developers experienced in class-based languages (like Java or C++), as it is dynamic and does not provide a <code>class</code> implementation per se (the <code>class</code> keyword is introduced in ES2015, but
        is syntactical sugar, JavaScript remains prototype-based).
    </p>
    <p>
        When it comes to inheritance, JavaScript only has one construct: objects. Each object has a private property (referred to as <code>[[Prototype]]</code> ) which holds a link to another object called its <strong>prototype</strong>. That prototype
        object has a prototype of its own, and so on until an object is reached with null as its prototype. By definition, null has no prototype, and acts as the final link in this <strong>prototype chain</strong>.
    </p>
    <p>
        While this is often considered to be one of JavaScript's weaknesses, the prototypal inheritance model is in fact more powerful than the classic model.&nbsp;It is, for example, fairly trivial to build a classic model on top of a prototypal model.
    </p>


    <!-- 'this' operator  start-->
    <div class='title'>Inheritance with the prototype chain</div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Inheriting properties</h3>
        </div>
        <div class="panel-body">
            JavaScript objects are dynamic "bags" of properties (referred to as own properties). JavaScript objects have a link to a prototype object. When trying to access a property of an object, the property will not only be sought on the object but on the prototype
            of the object, the prototype of the prototype, and so on until either a property with a matching name is found or the end of the prototype chain is reached.
        </div>
    </div>

    In most cases, the value of this is determined by how a function is called. It can't be set by assignment during execution, and it may be different each time the function is called. ES5 introduced the bind method to set the value of a function's this
    regardless of how it's called, and ES6 introduced arrow functions whose this is lexically scoped (it is set to the this value of the enclosing execution context).

    </br>
    </br>
    <div class="text-heading">Global context: </div>
    In the global execution context (outside of any function), this refers to the global object, whether in strict mode or not.

    </br>
    </br>
    <!-- 'this' operator  end-->
    <!-- Function context  start-->
    <div class="text-heading">Function context</div>
    Inside a function, the value of this depends on how the function is called.

    </br>
    <ul class="list">
        <li>Simple call:</li>
    </ul>

    <div style="  padding-left: 20px;">
        Since the following code is <code>not in strict mode</code>, and because the value of this is not set by the call, this will default to the global object:</li>

        <div class="snippet-container">
            <div class="sh_default snippet-wrap">
                <pre class="shi_pre sh_c snippet-formatted sh_sourceCode"><ol class="snippet-num"><li>    <span class="sh_usertype">function</span><span class="sh_normal"> </span><span class="sh_function">f1</span><span class="sh_symbol">()</span><span class="sh_cbracket">{</span></li><li>      <span class="sh_keyword">return</span> this<span class="sh_symbol">;</span></li><li>    <span class="sh_cbracket">}</span></li><li>    <span class="sh_comment">// In a browser:</span></li><li>    <span class="sh_function">f1</span><span class="sh_symbol">()</span> <span class="sh_symbol">===</span> window<span class="sh_symbol">;</span> <span class="sh_comment">// the window is the global object in browsers</span></li><li>    <span class="sh_comment">// In Node:</span></li><li>    <span class="sh_function">f1</span><span class="sh_symbol">()</span> <span class="sh_symbol">===</span> global</li></ol></pre>
                <pre class="snippet-textonly sh_sourceCode" style="display:none;" tabindex="0" contenteditable=""> function f1(){ return this; } // In a browser: f1() === window; // the window is the global object in browsers // In Node: f1() === global</pre>
            </div>
        </div>
        </br>
        In <code>strict mode</code>, however, the value of this remains at whatever it was set to when entering the execution context, so, in the following case, this will default to undefined:

        <div class="snippet-container">
            <div class="sh_default snippet-wrap">
                <pre class="shi_pre sh_javascript snippet-formatted sh_sourceCode"><ol class="snippet-num"><li><span class="sh_keyword">function</span> <span class="sh_function">f2</span><span class="sh_symbol">()</span><span class="sh_cbracket">{</span></li><li>  <span class="sh_string">"use strict"</span><span class="sh_symbol">;</span> <span class="sh_comment">// see strict mode</span></li><li>  <span class="sh_keyword">return</span> <span class="sh_keyword">this</span><span class="sh_symbol">;</span></li><li><span class="sh_cbracket">}</span></li><li></li><li><span class="sh_function">f2</span><span class="sh_symbol">()</span> <span class="sh_symbol">===</span> <span class="sh_predef_var">undefined</span><span class="sh_symbol">;</span></li></ol></pre>
                <pre class="snippet-textonly sh_sourceCode" style="display:none;" tabindex="0" contenteditable="">function f2(){ "use strict"; // see strict mode return this; } f2() === undefined;</pre>
            </div>
        </div>
        </br>
        So, in strict mode, if this was not defined by the execution context, it remains undefined.
    </div>
    </br>

    <!-- Function context  end-->
    <!-- call and apply  start-->
    <div class='title'>call and apply</div>
    <p>The <code><strong>call()</strong></code> method calls a function with a given <code>this</code> value and arguments provided individually.</p>
    </br>
    <p>The <code><strong>apply()</strong></code> method calls a function with a given <code>this</code> value and <code>arguments</code> provided as an array (or an <a href="/en-US/docs/Web/JavaScript/Guide/Indexed_collections#Working_with_array-like_objects">array-like object</a>).</p>

    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Note:</h3>
        </div>
        <div class="panel-body">
            While the syntax of this function is almost identical to that of call(), the fundamental difference is that call() accepts an argument list, while apply() accepts a single array of arguments.
        </div>
    </div>

    <div class="snippet-container">
        <div class="sh_default snippet-wrap">
            <pre class="shi_pre sh_javascript snippet-formatted sh_sourceCode"><ol class="snippet-num"><li><span class="sh_keyword">function</span> <span class="sh_function">add</span><span class="sh_symbol">(</span>c<span class="sh_symbol">,</span> d<span class="sh_symbol">)</span><span class="sh_cbracket">{</span></li><li>  <span class="sh_keyword">return</span> <span class="sh_keyword">this</span><span class="sh_symbol">.</span>a <span class="sh_symbol">+</span> <span class="sh_keyword">this</span><span class="sh_symbol">.</span>b <span class="sh_symbol">+</span> c <span class="sh_symbol">+</span> d<span class="sh_symbol">;</span></li><li><span class="sh_cbracket">}</span></li><li></li><li><span class="sh_keyword">var</span> o <span class="sh_symbol">=</span> <span class="sh_cbracket">{</span>a<span class="sh_symbol">:</span><span class="sh_number">1</span><span class="sh_symbol">,</span> b<span class="sh_symbol">:</span><span class="sh_number">3</span><span class="sh_cbracket">}</span><span class="sh_symbol">;</span></li><li></li><li><span class="sh_comment">// The first parameter is the object to use as</span></li><li><span class="sh_comment">// 'this', subsequent parameters are passed as </span></li><li><span class="sh_comment">// arguments in the function call</span></li><li>add<span class="sh_symbol">.</span><span class="sh_function">call</span><span class="sh_symbol">(</span>o<span class="sh_symbol">,</span> <span class="sh_number">5</span><span class="sh_symbol">,</span> <span class="sh_number">7</span><span class="sh_symbol">);</span> <span class="sh_comment">// 1 + 3 + 5 + 7 = 16</span></li><li></li><li><span class="sh_comment">// The first parameter is the object to use as</span></li><li><span class="sh_comment">// 'this', the second is an array whose</span></li><li><span class="sh_comment">// members are used as the arguments in the function call</span></li><li>add<span class="sh_symbol">.</span><span class="sh_function">apply</span><span class="sh_symbol">(</span>o<span class="sh_symbol">,</span> <span class="sh_symbol">[</span><span class="sh_number">10</span><span class="sh_symbol">,</span> <span class="sh_number">20</span><span class="sh_symbol">]);</span> <span class="sh_comment">// 1 + 3 + 10 + 20 = 34</span></li></ol></pre>
            <pre class="snippet-textonly sh_sourceCode" style="display:none;" tabindex="0" contenteditable="">function add(c, d){ return this.a + this.b + c + d; } var o = {a:1, b:3}; // The first parameter is the object to use as // 'this', subsequent parameters are passed as // arguments in the function call add.call(o, 5, 7); // 1 + 3 + 5 + 7 =
                16 // The first parameter is the object to use as // 'this', the second is an array whose // members are used as the arguments in the function call add.apply(o, [10, 20]); // 1 + 3 + 10 + 20 = 34</pre>
        </div>
    </div>
    </br>

    </br>

    <!-- call and apply  end-->
    <!-- bind method  start-->
    <div class='title'>The bind method</div>
    <p>The <code><strong>bind()</strong></code> method creates a new function that, when called, has its <code>this</code> keyword set to the provided value, with a given sequence of arguments preceding any provided when the new function is called.</p>
    </br>
    The.bind() method is used to permanently set the value of this inside the target function to the passed in context object.

    <div class="snippet-container">
        <div class="sh_default snippet-wrap">
            <pre class="shi_pre sh_javascript snippet-formatted sh_sourceCode"><ol class="snippet-num"><li><span class="sh_keyword">function</span> <span class="sh_function">f</span><span class="sh_symbol">()</span><span class="sh_cbracket">{</span></li><li>  <span class="sh_keyword">return</span> <span class="sh_keyword">this</span><span class="sh_symbol">.</span>a<span class="sh_symbol">;</span></li><li><span class="sh_cbracket">}</span></li><li></li><li><span class="sh_keyword">var</span> g <span class="sh_symbol">=</span> f<span class="sh_symbol">.</span><span class="sh_function">bind</span><span class="sh_symbol">(</span><span class="sh_cbracket">{</span>a<span class="sh_symbol">:</span><span class="sh_string">"azerty"</span><span class="sh_cbracket">}</span><span class="sh_symbol">);</span></li><li>console<span class="sh_symbol">.</span><span class="sh_function">log</span><span class="sh_symbol">(</span><span class="sh_function">g</span><span class="sh_symbol">());</span> <span class="sh_comment">// azerty</span></li><li></li><li><span class="sh_keyword">var</span> h <span class="sh_symbol">=</span> g<span class="sh_symbol">.</span><span class="sh_function">bind</span><span class="sh_symbol">(</span><span class="sh_cbracket">{</span>a<span class="sh_symbol">:</span><span class="sh_string">"yoo"</span><span class="sh_cbracket">}</span><span class="sh_symbol">);</span> <span class="sh_comment">// bind only works once!</span></li><li>console<span class="sh_symbol">.</span><span class="sh_function">log</span><span class="sh_symbol">(</span><span class="sh_function">h</span><span class="sh_symbol">());</span> <span class="sh_comment">// azerty</span></li><li></li><li><span class="sh_keyword">var</span> o <span class="sh_symbol">=</span> <span class="sh_cbracket">{</span>a<span class="sh_symbol">:</span><span class="sh_number">37</span><span class="sh_symbol">,</span> f<span class="sh_symbol">:</span>f<span class="sh_symbol">,</span> g<span class="sh_symbol">:</span>g<span class="sh_symbol">,</span> h<span class="sh_symbol">:</span>h<span class="sh_cbracket">}</span><span class="sh_symbol">;</span></li><li>console<span class="sh_symbol">.</span><span class="sh_function">log</span><span class="sh_symbol">(</span>o<span class="sh_symbol">.</span><span class="sh_function">f</span><span class="sh_symbol">(),</span> o<span class="sh_symbol">.</span><span class="sh_function">g</span><span class="sh_symbol">(),</span> o<span class="sh_symbol">.</span><span class="sh_function">h</span><span class="sh_symbol">());</span> <span class="sh_comment">// 37, azerty, azerty</span></li></ol></pre>
            <pre class="snippet-textonly sh_sourceCode" style="display:none;" tabindex="0" contenteditable="">function f(){ return this.a; } var g = f.bind({a:"azerty"}); console.log(g()); // azerty var h = g.bind({a:"yoo"}); // bind only works once! console.log(h()); // azerty var o = {a:37, f:f, g:g, h:h}; console.log(o.f(), o.g(), o.h()); // 37,
                azerty, azerty</pre>
        </div>
    </div>
    </br>
    <p>The <strong>bind()</strong> function creates a new <strong>bound function</strong> <strong>(BF)</strong>. A <strong>BF</strong> is an <strong>exotic function object</strong> (a term from <strong>ECMAScript 2015</strong>) that wraps the original function
        object. Calling a <strong>BF</strong> generally, results in the execution of its <strong>wrapped function</strong>.<br> A
        <strong> BF</strong> has the following internal properties:
    </p>

    <ul class="list">
        <li><strong>[[BoundTargetFunction]] </strong>- the wrapped function object;</li>
        <li><strong>[[BoundThis]]</strong> - the value that is always passed as <strong>this</strong> value when calling the wrapped function.</li>
        <li><strong>[[BoundArguments]]</strong> - a list of values whose elements are used as the first arguments to any call to the wrapped function.</li>
        <li><strong>[[Call]]</strong> - executes code associated with this object. Invoked via a function call expression. The arguments to the internal method are a <strong>this</strong> value and a list containing the arguments passed to the function by
            a call expression.</li>
    </ul>

    <p>When bound function is called, it calls internal method<strong> [[Call]]</strong> with following arguments <strong>Call(<em>target</em>, <em>boundThis</em>, <em>args</em>).</strong> Where, <strong><em>target</em></strong> is<strong> [[BoundTargetFunction]]</strong>,
        <strong><em>boundThis </em></strong>is <strong>[[BoundThis]]</strong>, <em><strong>args </strong></em>is <strong>[[BoundArguments]]</strong>.
    </p>

    <p>A bound function may also be constructed using the <a href="/en-US/docs/Web/JavaScript/Reference/Operators/new" title="The new operator creates an instance of a user-defined object type or of one of the built-in object types that has a constructor function."><code>new</code></a>        operator: doing so acts as though the target function had instead been constructed. The provided <strong><code>this</code></strong> value is ignored, while prepended arguments are provided to the emulated function.</p>

    <!-- bind method  end-->


    <!-- Arrow functions  start-->
    </br>
    <div class='title'>Arrow functions</div>

    <p>An <strong>arrow function expression</strong> has a shorter syntax than a&nbsp;<a href="/en-US/docs/Web/JavaScript/Reference/Operators/function">function expression</a>&nbsp;and does not bind&nbsp;its own&nbsp;<code><a href="/en-US/docs/Web/JavaScript/Reference/Operators/this">this</a></code>,&nbsp;
        <a href="/en-US/docs/Web/JavaScript/Reference/Functions/arguments">arguments</a>,&nbsp;<a href="/en-US/docs/Web/JavaScript/Reference/Operators/super">super</a>, or&nbsp;<a href="/en-US/docs/Web/JavaScript/Reference/Operators/new.target">new.target</a>.
        Arrow functions are always <a href="/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/name">anonymous</a>. These function expressions are best suited for non-method functions, and they cannot be used as constructors.</p>
    </br>
    <p>There is one subtle difference in behavior between ordinary <code>function</code> functions and arrow functions. <strong>Arrow functions do not have their own <code>this</code> value.</strong> The value of <code>this</code> inside an arrow function
        is always inherited from the enclosing scope.</p>
    </br>
    <div class="snippet-container">
        <div class="sh_default snippet-wrap">
            <pre class="shi_pre sh_javascript snippet-formatted sh_sourceCode"><ol class="snippet-num"><li><span class="sh_symbol">(</span>param1<span class="sh_symbol">,</span> param2<span class="sh_symbol">,</span> …<span class="sh_symbol">,</span> paramN<span class="sh_symbol">)</span> <span class="sh_symbol">=&gt;</span> <span class="sh_cbracket">{</span> statements <span class="sh_cbracket">}</span></li><li><span class="sh_symbol">(</span>param1<span class="sh_symbol">,</span> param2<span class="sh_symbol">,</span> …<span class="sh_symbol">,</span> paramN<span class="sh_symbol">)</span> <span class="sh_symbol">=&gt;</span> expression</li><li><span class="sh_comment">// equivalent to: (param1, param2, …, paramN) =&gt; { return expression; }</span></li><li></li><li><span class="sh_comment">// Parentheses are optional when there's only one parameter:</span></li><li><span class="sh_symbol">(</span>singleParam<span class="sh_symbol">)</span> <span class="sh_symbol">=&gt;</span> <span class="sh_cbracket">{</span> statements <span class="sh_cbracket">}</span></li><li>singleParam <span class="sh_symbol">=&gt;</span> <span class="sh_cbracket">{</span> statements <span class="sh_cbracket">}</span></li><li></li><li><span class="sh_comment">// A function with no parameters requires parentheses:</span></li><li><span class="sh_symbol">()</span> <span class="sh_symbol">=&gt;</span> <span class="sh_cbracket">{</span> statements <span class="sh_cbracket">}</span></li><li><span class="sh_symbol">()</span> <span class="sh_symbol">=&gt;</span> expression <span class="sh_comment">// equivalent to: () =&gt; { return expression; }</span></li></ol></pre>
            <pre class="snippet-textonly sh_sourceCode" style="display:none;" tabindex="0" contenteditable="">(param1, param2, …, paramN) =&gt; { statements } (param1, param2, …, paramN) =&gt; expression // equivalent to: (param1, param2, …, paramN) =&gt; { return expression; } // Parentheses are optional when there's only one parameter: (singleParam)
                =&gt; { statements } singleParam =&gt; { statements } // A function with no parameters requires parentheses: () =&gt; { statements } () =&gt; expression // equivalent to: () =&gt; { return expression; }</pre>
        </div>
    </div>

    <!-- Arrow functions  end-->

</div>

</br>
