(function() {
  'use strict';
  
  angular
    .module('nodejs')
    .controller('nodejsController', nodejsController);
    
    function nodejsController() {
      this.data = 'the data'; // use {{vm.data}} in the view
    }
}());