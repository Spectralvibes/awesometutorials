(function() {
  'use strict';
  
  angular
    .module('dashboard')
    .controller('dashboardController', dashboardController);
    
    function dashboardController() {
      this.data = 'the data'; // use {{vm.data}} in the view
    }
}());

