<!-- --------------------------------------------------- Introduction start --------------------------------------------------- -->
<div class="alert alert-danger title">Introduction</div>
<div class='text'>
    <p>The AngularJS framework encourages writing clean, solid, testable code. This feature is one of the most useful that you get out of the box with Angular.</p>
    <p>The Angular team’s emphasis on testing is so strong that they built a test runner to make the process easier. In their words:</p>
    <div class='alert alert-warning'>
        JavaScript is a dynamically typed language which comes with great power of expression, but it also comes with almost no help from the compiler. For this reason we feel very strongly that any code written in JavaScript needs to come with a strong set of
        tests. We have built many features into Angular which makes testing your Angular applications easy. So there is no excuse for not testing.
    </div>
</div>
<div class='text'>
    <div class='text-heading'>Why test?</div>
    <p>
        From personal experience, tests are the best way to prevent software defects. I've been on many teams in the past where a small piece of code is updated and the developer manually opens their browser or Postman to verify that it still works. This approach
        (manual QA) is begging for a disaster.
    </p>
    <p>
        <strong>As features and codebases grow, manual QA becomes more expensive, time consuming, and error prone.</strong> If a feature or function is removed does every developer remember all of its potential side-effects? Are all developers manually
        testing in the same way? Probably not.
    </p>

</div>
<!-- --------------------------------------------------- Introduction end ----------------------------------------------------- -->

<div class='text-heading'>Tools for testing AngularJS applications</div>
<p>For testing AngularJS applications there are certain tools that you should use that will make testing much easier to set up and run.</p>

<!-- --------------------------------------------------- Karma start --------------------------------------------------- -->
<div class="alert alert-danger title">Karma</div>

<div class='text'>
    <p>
        Karma is a JavaScript command line tool that can be used to spawn a web server which loads your application's source code and executes your tests. You can configure Karma to run against a number of browsers, which is useful for being confident that your
        application works on all browsers you need to support. Karma is executed on the command line and will display the results of your tests on the command line once they have run in the browser.
    </p>
    <p>Karma is a NodeJS application, and should be installed through npm/yarn. Full installation instructions are available on <a href="http://karma-runner.github.io/0.12/intro/installation.html">the Karma website</a>.</p>
    <div class='text-heading'>Karma setup:</div>
    <div class="snippet-container">
        <div class="sh_default snippet-wrap">
            <pre class="shi_pre sh_javascript snippet-formatted sh_sourceCode"><ol class="snippet-num"><li><span class="sh_comment">// Install karma and jasmine using npm</span></li><li>npm install karma karma<span class="sh_symbol">-</span>jasmine jasmine<span class="sh_symbol">-</span>core</li><li><span class="sh_comment">// Configure karma</span></li><li>karma init</li></ol></pre>
            <pre class="snippet-textonly sh_sourceCode" style="display:none;" tabindex="0" contenteditable="">// Install karma and jasmine using npm npm install karma karma-jasmine jasmine-core // Configure karma karma init</pre>
        </div>
    </div>

    <div class='text-heading'>Karma configuration:</div>
    <p>Karma config file (karma.conf.js) contains property array for list of files.</p>
    <div class="snippet-container">
        <div class="sh_default snippet-wrap">
            <pre class="shi_pre sh_javascript_dom snippet-formatted sh_sourceCode"><ol class="snippet-num"><li>    <span class="sh_comment">// list of files / patterns to load in the browser</span></li><li>    files<span class="sh_symbol">:</span> <span class="sh_symbol">[</span></li><li>      <span class="sh_string">'public/bower/angular/angular.min.js'</span><span class="sh_symbol">,</span></li><li>      <span class="sh_string">'public/bower/angular-mocks/angular-mocks.js'</span><span class="sh_symbol">,</span></li><li>      <span class="sh_string">'test/component/app/*'</span></li><li>    <span class="sh_symbol">],</span></li></ol></pre>
            <pre class="snippet-textonly sh_sourceCode" style="display:none;" tabindex="0" contenteditable=""> // list of files / patterns to load in the browser files: [ 'public/bower/angular/angular.min.js', 'public/bower/angular-mocks/angular-mocks.js', 'test/component/app/*' ],
                </pre>
        </div>
    </div>

</div>
<!-- --------------------------------------------------- Karma end ----------------------------------------------------- -->

<!-- --------------------------------------------------- Jasmine start --------------------------------------------------- -->
<div class="alert alert-danger title">Jasmine</div>
<div class='text'>
    <p>
        <a href="http://jasmine.github.io/1.3/introduction.html">Jasmine</a> is a behavior driven development framework for JavaScript that has become the most popular choice for testing AngularJS applications. Jasmine provides functions to help with
        structuring your tests and also making assertions. As your tests grow, keeping them well structured and documented is vital, and Jasmine helps achieve this.
    </p>
    <p>In Jasmine we use the <code><span class="pln">describe</span></code> function to group our tests together:</p>
    <p>
        When we're using Jasmine to test our code we group our tests together with what Jasmine calls a <a href="http://jasmine.github.io/2.4/introduction.html#section-Standalone_Distribution">"test suite"</a>. We begin our test suite by calling Jasmine's
        global <code>describe</code> function. This function takes two parameters: a string and a function. The string serves as a title and the function is the code that implements our tests.
    </p>
    <div class="snippet-container">
        <div class="sh_default snippet-wrap">
            <pre class="shi_pre sh_javascript_dom snippet-formatted sh_sourceCode"><ol class="snippet-num"><li><span class="sh_function">describe</span><span class="sh_symbol">(</span><span class="sh_string">'sorting the list of users'</span><span class="sh_symbol">,</span> <span class="sh_keyword">function</span><span class="sh_symbol">()</span> <span class="sh_cbracket">{</span></li><li>  <span class="sh_function">it</span><span class="sh_symbol">(</span><span class="sh_string">'sorts in descending order by default'</span><span class="sh_symbol">,</span> <span class="sh_keyword">function</span><span class="sh_symbol">()</span> <span class="sh_cbracket">{</span></li><li>    <span class="sh_keyword">var</span> users <span class="sh_symbol">=</span> <span class="sh_symbol">[</span><span class="sh_string">'jack'</span><span class="sh_symbol">,</span> <span class="sh_string">'igor'</span><span class="sh_symbol">,</span> <span class="sh_string">'jeff'</span><span class="sh_symbol">];</span></li><li>    <span class="sh_keyword">var</span> sorted <span class="sh_symbol">=</span> <span class="sh_function">sortUsers</span><span class="sh_symbol">(</span>users<span class="sh_symbol">);</span></li><li>    <span class="sh_function">expect</span><span class="sh_symbol">(</span>sorted<span class="sh_symbol">).</span><span class="sh_function">toEqual</span><span class="sh_symbol">([</span><span class="sh_string">'jeff'</span><span class="sh_symbol">,</span> <span class="sh_string">'jack'</span><span class="sh_symbol">,</span> <span class="sh_string">'igor'</span><span class="sh_symbol">]);</span></li><li>  <span class="sh_cbracket">}</span><span class="sh_symbol">);</span></li><li><span class="sh_cbracket">}</span><span class="sh_symbol">);</span></li></ol></pre>
            <pre class="snippet-textonly sh_sourceCode" style="display:none;" tabindex="0" contenteditable="">describe('sorting the list of users', function() { it('sorts in descending order by default', function() { var users = ['jack', 'igor', 'jeff']; var sorted = sortUsers(users); expect(sorted).toEqual(['jeff', 'jack', 'igor']); }); });
                </pre>
        </div>
    </div>
    <p>
        Within this describe block we'll add specs. Specs look similar to suites since they take a string and a function as arguments but rather than call <code>describe</code> we're going to call <code>it</code> (individual test) instead. Within our
        <code>it</code> block is where we put our expecations that tests our code.
    </p>

    <p>Jasmine comes with a number of matchers that help you make a variety of assertions.</p>

    <div class='text-heading'>A suite and spec:</div>
    <div class="snippet-container">
        <div class="sh_default snippet-wrap">
            <pre class="shi_pre sh_javascript_dom snippet-formatted sh_sourceCode"><ol class="snippet-num"><li><span class="sh_function">describe</span><span class="sh_symbol">(</span><span class="sh_string">"A suite"</span><span class="sh_symbol">,</span> <span class="sh_keyword">function</span><span class="sh_symbol">()</span> <span class="sh_cbracket">{</span></li><li>  <span class="sh_function">it</span><span class="sh_symbol">(</span><span class="sh_string">"contains spec with an expectation"</span><span class="sh_symbol">,</span> <span class="sh_keyword">function</span><span class="sh_symbol">()</span> <span class="sh_cbracket">{</span></li><li>    <span class="sh_function">expect</span><span class="sh_symbol">(</span><span class="sh_keyword">true</span><span class="sh_symbol">).</span><span class="sh_function">toBe</span><span class="sh_symbol">(</span><span class="sh_keyword">true</span><span class="sh_symbol">);</span></li><li>  <span class="sh_cbracket">}</span><span class="sh_symbol">);</span></li><li><span class="sh_cbracket">}</span><span class="sh_symbol">);</span></li></ol></pre>
            <pre class="snippet-textonly sh_sourceCode" style="display:none;" tabindex="0" contenteditable="">describe("A suite", function() { it("contains spec with an expectation", function() { expect(true).toBe(true); }); });
                </pre>
        </div>
    </div>

    <div class='text-heading'>It's Just Functions:</div>
    <p>Since <code>describe</code> and <code>it</code> blocks are functions, they can contain any executable code necessary to implement the test. JavaScript scoping rules apply, so variables declared in a <code>describe</code> are available to any <code>it</code>        block inside the suite.</p>
    <div class="snippet-container">
        <div class="sh_default snippet-wrap">
            <div class="snippet-menu sh_sourceCode"><a class="snippet-window sh_url" href="#">pop-up</a><a class="snippet-text sh_url" href="#">text</a></div><pre class="shi_pre sh_javascript_dom snippet-formatted sh_sourceCode"><ol class="snippet-num"><li><span class="sh_function">describe</span><span class="sh_symbol">(</span><span class="sh_string">"A suite is just a function"</span><span class="sh_symbol">,</span> <span class="sh_keyword">function</span><span class="sh_symbol">()</span> <span class="sh_cbracket">{</span></li><li>  <span class="sh_keyword">var</span> a<span class="sh_symbol">;</span></li><li>  <span class="sh_function">it</span><span class="sh_symbol">(</span><span class="sh_string">"and so is a spec"</span><span class="sh_symbol">,</span> <span class="sh_keyword">function</span><span class="sh_symbol">()</span> <span class="sh_cbracket">{</span></li><li>    a <span class="sh_symbol">=</span> <span class="sh_keyword">true</span><span class="sh_symbol">;</span></li><li>    <span class="sh_function">expect</span><span class="sh_symbol">(</span>a<span class="sh_symbol">).</span><span class="sh_function">toBe</span><span class="sh_symbol">(</span><span class="sh_keyword">true</span><span class="sh_symbol">);</span></li><li>  <span class="sh_cbracket">}</span><span class="sh_symbol">);</span></li><li><span class="sh_cbracket">}</span><span class="sh_symbol">);</span></li></ol></pre>
            <pre class="snippet-textonly sh_sourceCode" style="display:none;" tabindex="0" contenteditable="">describe("A suite is just a function", function() { var a; it("and so is a spec", function() { a = true; expect(a).toBe(true); }); });
                </pre>
        </div>
    </div>

    <div class='text-heading'>
        <p>Please <a href="http://jasmine.github.io/1.3/introduction.html#section-Matchers">read the Jasmine documentation</a> to get more details. </p>
    </div>


</div>
<!-- --------------------------------------------------- Jasmine end ----------------------------------------------------- -->

<!-- --------------------------------------------------- start --------------------------------------------------- -->
<div class="alert alert-danger title">E2E testing - protractor</div>
<div class='text'>
    -- please update here... --
</div>
<!-- --------------------------------------------------- end ----------------------------------------------------- -->

<!-- --------------------------------------------------- start --------------------------------------------------- -->
<!-- --------------------------------------------------- end ----------------------------------------------------- -->
