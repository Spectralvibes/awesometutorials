(function() {
  'use strict';

  angular
    .module('angularjs')
    .controller('angularjsController', ['$scope', 'snakeCase', '$location', function($scope, snakeCase, $location) {

      $scope.getClass = function(path) {
        return ($location.path().substr(0, path.length) === path) ? 'active' : '';
      }

      $scope.snakeCase = function(msg) {
        return snakeCase(msg);
      };


      $scope.tracker = {
        introduction: 90,
        dataBinding: 90,
        controllers: 40,
        services: 70,
        scopes: 50,
        dependencyInjection: 60,
        templates: 70,
        expressions: 30,
        interpolation: 40,
        filters: 30,
        forms: 30,
        directives: 70,
        components: 10,
        componentRouter: 10,
        animations: 10,
        modules: 20,
        htmlCompiler: 60,
        providers: 80,
        decorators: 70,
        bootstrap: 40,
        unitTesting: 40,
        e2eTesting: 10,
        using$location: 10,
        workingWithCss: 20,
        i18nAndL10n: 10,
        seurity: 10,
        accessibility: 10,
        internetExplorerCompatibility: 10,
        runningInProduction: 10,
        migratingFromPreviousVersions: 10
      };

      $scope.randomProperty = pickRandomProperty($scope.tracker);
      $scope.randomPropertyProgress = $scope.tracker[$scope.randomProperty];

      function pickRandomProperty(obj) {
        var result;
        var count = 0;
        for (var prop in obj)
          if (Math.random() < 1 / ++count)
            result = prop;
        return result;
      }

    }])
    .service('snakeCase', function() {


      function capitalise(string) {
        return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
      }

      function lowerFirst(string) {
        return string.replace(/^[A-Z]/, function(m) {
          return m.toLowerCase();
        });
      }

      function trimCase(string) {
        return string.replace(/^[\s\-_]+|[\s\-_]+$/g, '');
      }

      function sentenceCase(string) {
        return capitalise(kebabCase(string).replace(/(-)/g, ' '));
      }

      function kebabCase(string) {
        return lowerFirst(string).replace(/([A-Z])/g, function(m, g) {
          return '-' + g.toLowerCase();
        }).replace(/[\s\-_]+/g, '-');
      }

      return (items) => {
        this.out = sentenceCase(trimCase(items));
        return this.out;
      }

    })
    .filter('snakeCase', function() {

      return function(items) {

        var input = Object.keys(items)
        var out;

        function capitalise(string) {
          return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
        }

        function lowerFirst(string) {
          return string.replace(/^[A-Z]/, function(m) {
            return m.toLowerCase();
          });
        }

        function trimCase(string) {
          return string.replace(/^[\s\-_]+|[\s\-_]+$/g, '');
        }

        function sentenceCase(string) {
          return capitalise(kebabCase(string).replace(/(-)/g, ' '));
        }

        function kebabCase(string) {
          return lowerFirst(string).replace(/([A-Z])/g, function(m, g) {
            return '-' + g.toLowerCase();
          }).replace(/[\s\-_]+/g, '-');
        }

        var temp;
        var key;
        for (var i = 0; i < input.length; i++) {
          key = Object.keys(i);
          temp = items.key;
          console.log(temp);
        }
        return sentenceCase(trimCase(Object.keys(items)));

      };
    })

}());
