(function() {
  'use strict';
  
  angular
    .module('tryit')
    .controller('tryitController', tryitController);
    
    tryitController.$inject=['$location'];
    
    function tryitController($location) {
      this.data = 'the data'; // use {{vm.data}} in the view
      
      console.log('Log starts here');
      console.log($location.path());
      
    }
}());