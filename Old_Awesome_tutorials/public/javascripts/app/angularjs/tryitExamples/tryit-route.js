(function() {
    'use strict';

    angular
        .module('tryit', ['ngRoute'])
        .config(config);

    config.$inject = ['$routeProvider'];

    function config($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '/javascripts/app/angularjs/tryitExamples/01.html',
                controller: 'tryitController',
                controllerAs: 'vm'
            })
            .when('/01', {
                templateUrl: '/javascripts/app/angularjs/tryitExamples/01.html',
                controller: 'tryitController',
                controllerAs: 'vm'
            })
            .when('/02environment', {
                templateUrl: '/javascripts/app/angularjs/02environment.html',
                controller: 'angularjsController',
                controllerAs: 'vm'
            });
    }
}());
