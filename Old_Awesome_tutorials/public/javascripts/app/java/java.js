(function() {
  'use strict';

  angular
    .module('javaModule')
    .controller('javaController', ['$scope', 'snakeCase', function($scope, snakeCase) {

      $scope.snakeCase = function(msg) {
        return snakeCase(msg);
      };

      $scope.tracker = {
        Angularjs: 90,
        NodeJS: 90,
        JavaScript: 40,
        HTML: 70,
        CSS: 50,
        jQuery: 60,
        Subject: 70,
        Subject: 30,
        Subject: 40
      };

      $scope.randomProperty = pickRandomProperty($scope.tracker);
      $scope.randomPropertyProgress = $scope.tracker[$scope.randomProperty];

      function pickRandomProperty(obj) {
        var result;
        var count = 0;
        for (var prop in obj)
          if (Math.random() < 1 / ++count)
            result = prop;
        return result;
      }

    }])
    .service('snakeCase', function() {


      function capitalise(string) {
        return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
      }

      function lowerFirst(string) {
        return string.replace(/^[A-Z]/, function(m) {
          return m.toLowerCase();
        });
      }

      function trimCase(string) {
        return string.replace(/^[\s\-_]+|[\s\-_]+$/g, '');
      }

      function sentenceCase(string) {
        return capitalise(kebabCase(string).replace(/(-)/g, ' '));
      }

      function kebabCase(string) {
        return lowerFirst(string).replace(/([A-Z])/g, function(m, g) {
          return '-' + g.toLowerCase();
        }).replace(/[\s\-_]+/g, '-');
      }

      return (items) => {
        this.out = sentenceCase(trimCase(items));
        return this.out;
      }

    })

}());
