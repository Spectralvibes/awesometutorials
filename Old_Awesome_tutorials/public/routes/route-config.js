(function() {
        'use strict';

        angular
            .module('dashboard',['ngRoute'])
            .config(config);

        config.$inject = ['$routeProvider'];

        function config($routeProvider) {
            $routeProvider
                .when('/dashboard', {
                    templateUrl: '/javascripts/app/dashboard/dashboard.html',
                    controller: 'dashboardController',
                    controllerAs: 'vm'
                })
                .when('/', {
                    templateUrl: '/javascripts/app/dashboard/dashboard.html',
                    controller: 'dashboardController',
                    controllerAs: 'vm'
                })
                .when('/nodejs', {
                    templateUrl: '/javascripts/app/nodejs/dashboard.html'
                });
        }
}());