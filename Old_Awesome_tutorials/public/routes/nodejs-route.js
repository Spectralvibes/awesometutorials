(function() {
    'use strict';

    angular
        .module('nodejs', ['ngRoute'])
        .config(config);

    config.$inject = ['$routeProvider'];

    function config($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '/javascripts/app/nodejs/01introduction.html',
                controller: 'nodejsController',
                controllerAs: 'vm'
            })
            .when('/01introduction', {
                templateUrl: '/javascripts/app/nodejs/01introduction.html',
                controller: 'nodejsController',
                controllerAs: 'vm'
            });
    }
}());
