(function() {
    'use strict';

    angular
        .module('angularjs', ['ngRoute'])
        .config(config);

    config.$inject = ['$routeProvider'];

    function config($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '/javascripts/app/angularjs/01introduction.html',
                controller: 'angularjsController',
                controllerAs: 'vm'
            })
            .when('/01introduction', {
                templateUrl: '/javascripts/app/angularjs/01introduction.html',
                controller: 'angularjsController',
                controllerAs: 'vm'
            })
            .when('/controller', {
                templateUrl: '/javascripts/app/angularjs/controller.html',
                controller: 'angularjsController',
                controllerAs: 'vm'
            })
            .when('/modules', {
                templateUrl: '/javascripts/app/angularjs/modules.html',
                controller: 'angularjsController',
                controllerAs: 'vm'
            })
            .when('/builtindirectives', {
                templateUrl: '/javascripts/app/angularjs/builtindirectives.html',
                controller: 'angularjsController',
                controllerAs: 'vm'
            })
            .when('/02directives', {
                templateUrl: '/javascripts/app/angularjs/02directives.html',
                controller: 'angularjsController',
                controllerAs: 'vm'
            })
            .when('/Asynchronous', {
                templateUrl: '/javascripts/app/angularjs/Asynchronous.html',
                controller: 'angularjsController',
                controllerAs: 'vm'
            })
            .when('/Services', {
                templateUrl: '/javascripts/app/angularjs/Services.html',
                controller: 'angularjsController',
                controllerAs: 'vm'
            })
            .when('/builtInServices', {
                templateUrl: '/javascripts/app/angularjs/builtInServices.html',
                controller: 'angularjsController',
                controllerAs: 'vm'
            })
            .when('/unitTesting', {
                templateUrl: '/javascripts/app/angularjs/unitTesting.html',
                controller: 'angularjsController',
                controllerAs: 'vm'
            })
            .when('/Tracker', {
                templateUrl: '/javascripts/app/angularjs/Tracker.html',
                controller: 'angularjsController',
                controllerAs: 'vm'
            })
            .when('/Filters', {
                templateUrl: '/javascripts/app/angularjs/Filters.html',
                controller: 'angularjsController',
                controllerAs: 'vm'
            })
            .when('/styleGuide', {
                templateUrl: '/javascripts/app/angularjs/styleGuide.html',
                controller: 'angularjsController',
                controllerAs: 'vm'
            });
    }
}());
