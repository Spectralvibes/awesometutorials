(function() {
    'use strict';

    angular
        .module('javaModule', ['ngRoute'])
        .config(config);

    config.$inject = ['$routeProvider'];

    function config($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '/javascripts/app/java/01_Introduction.html',
                controller: 'javaController',
                controllerAs: 'vm'
            })
            .when('/01_Introduction', {
                templateUrl: '/javascripts/app/java/01_Introduction.html',
                controller: 'javaController',
                controllerAs: 'vm'
            })
            .when('/controller', {
                templateUrl: '/javascripts/app/angularjs/controller.html',
                controller: 'javaController',
                controllerAs: 'vm'
            })
            .when('/modules', {
                templateUrl: '/javascripts/app/angularjs/modules.html',
                controller: 'javaController',
                controllerAs: 'vm'
            })
            .when('/builtindirectives', {
                templateUrl: '/javascripts/app/angularjs/builtindirectives.html',
                controller: 'javaController',
                controllerAs: 'vm'
            })
            .when('/02directives', {
                templateUrl: '/javascripts/app/angularjs/02directives.html',
                controller: 'javaController',
                controllerAs: 'vm'
            })
            .when('/Asynchronous', {
                templateUrl: '/javascripts/app/angularjs/Asynchronous.html',
                controller: 'javaController',
                controllerAs: 'vm'
            })
            .when('/Services', {
                templateUrl: '/javascripts/app/angularjs/Services.html',
                controller: 'javaController',
                controllerAs: 'vm'
            })
            .when('/unitTesting', {
                templateUrl: '/javascripts/app/angularjs/unitTesting.html',
                controller: 'javaController',
                controllerAs: 'vm'
            })
            .when('/Tracker', {
                templateUrl: '/javascripts/app/angularjs/Tracker.html',
                controller: 'javaController',
                controllerAs: 'vm'
            })
            .when('/Filters', {
                templateUrl: '/javascripts/app/angularjs/Filters.html',
                controller: 'javaController',
                controllerAs: 'vm'
            })
            .when('/collections', {
                templateUrl: '/javascripts/app/java/collections.html',
                controller: 'javaController',
                controllerAs: 'vm'
            });
    }
}());
