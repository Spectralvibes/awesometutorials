(function() {
    'use strict';

    angular
        .module('interview', ['ngRoute'])
        .config(config);

    config.$inject = ['$routeProvider'];

    function config($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '/javascripts/app/interview/Introduction.html',
                controller: 'interviewController',
                controllerAs: 'vm'
            })
            .when('/JavaScript', {
                templateUrl: '/javascripts/app/interview/JavaScript.html',
                controller: 'interviewController',
                controllerAs: 'vm'
            })
            .when('/HTML', {
                templateUrl: '/javascripts/app/interview/HTML.html',
                controller: 'interviewController',
                controllerAs: 'vm'
            })
            .when('/jQuery', {
                templateUrl: '/javascripts/app/interview/jQuery.html',
                controller: 'interviewController',
                controllerAs: 'vm'
            })
            .when('/CSS', {
                templateUrl: '/javascripts/app/interview/CSS.html',
                controller: 'interviewController',
                controllerAs: 'vm'
            })
            .when('/AngularJS', {
                templateUrl: '/javascripts/app/interview/AngularJS.html',
                controller: 'interviewController',
                controllerAs: 'vm'
            })
            .when('/NodeJS', {
                templateUrl: '/javascripts/app/interview/NodeJS.html',
                controller: 'interviewController',
                controllerAs: 'vm'
            });

    }
}());
