(function() {
    'use strict';

    angular
        .module('javascript', ['ngRoute'])
        .config(config);

    config.$inject = ['$routeProvider'];

    function config($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '/javascripts/app/javascript/01introduction.html',
                controller: 'javascriptController',
                controllerAs: 'vm'
            })
            .when('/DOM', {
                templateUrl: '/javascripts/app/javascript/DOM.html',
                controller: 'javascriptController',
                controllerAs: 'vm'
            })
            .when('/BOM', {
                templateUrl: '/javascripts/app/javascript/BOM.html',
                controller: 'javascriptController',
                controllerAs: 'vm'
            })
            .when('/HTMLObjects', {
                templateUrl: '/javascripts/app/javascript/HTMLObjects.html',
                controller: 'javascriptController',
                controllerAs: 'vm'
            })
            .when('/01introduction', {
                templateUrl: '/javascripts/app/javascript/01introduction.html',
                controller: 'javascriptController',
                controllerAs: 'vm'
            })
            .when('/02directives', {
                templateUrl: '/javascripts/app/javascript/02directives.html',
                controller: 'javascriptController',
                controllerAs: 'vm'
            })
            .when('/this', {
                templateUrl: '/javascripts/app/javascript/this.html',
                controller: 'javascriptController',
                controllerAs: 'vm'
            })
            .when('/codePractice', {
                templateUrl: '/javascripts/app/javascript/codePractice.html',
                controller: 'javascriptController',
                controllerAs: 'vm'
            })
            .when('/Inheritance', {
                templateUrl: '/javascripts/app/javascript/Inheritance.html',
                controller: 'javascriptController',
                controllerAs: 'vm'
            });

    }
}());
