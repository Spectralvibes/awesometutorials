(function() {
    'use strict';

    angular
        .module('jquery', ['ngRoute'])
        .config(config);

    config.$inject = ['$routeProvider'];

    function config($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '/javascripts/app/jquery/Introduction.html',
                controller: 'jqueryController',
                controllerAs: 'vm'
            })
            .when('/Introduction', {
                templateUrl: '/javascripts/app/jquery/Introduction.html',
                controller: 'jqueryController',
                controllerAs: 'vm'
            })
            .when('/Selectors', {
                templateUrl: '/javascripts/app/jquery/Selectors.html',
                controller: 'jqueryController',
                controllerAs: 'vm'
            })
            .when('/NoConflict', {
                templateUrl: '/javascripts/app/jquery/NoConflict.html',
                controller: 'jqueryController',
                controllerAs: 'vm'
            })
            .when('/DOMManipulation', {
                templateUrl: '/javascripts/app/jquery/DOMManipulation.html',
                controller: 'jqueryController',
                controllerAs: 'vm'
            });


    }
}());
